require adurl
require busy

epicsEnvSet("IOC",		"iocURLTest")
epicsEnvSet("TOP",		".")
epicsEnvSet("PREFIX", "URL1:")

epicsEnvSet("EPICS_CA_MAX_ARRAY_BYTES","64000000")

### The port name for the detector
epicsEnvSet("PORT",   "URL1")
### Queue size
epicsEnvSet("QSIZE",  "5")   
### The maximim image width; used for row profiles in the NDPluginStats plugin
epicsEnvSet("XSIZE",  "2048")
### The maximim image height; used for column profiles in the NDPluginStats plugin
epicsEnvSet("YSIZE",  "1556")
### The maximum number of time series points in the NDPluginStats plugin
epicsEnvSet("NCHANS", "2048")
### The maximum number of frames buffered in the NDPluginCircularBuff plugin
epicsEnvSet("CBUFFS", "500")
### The search path for database files

### Define NELEMENTS 
epicsEnvSet("NELEMENTS", "12582912")

# Create a URL driver
# URLDriverConfig(const char *portName, int maxBuffers, size_t maxMemory,
#                 int priority, int stackSize)
URLDriverConfig("$(PORT)", 0, 0)
dbLoadRecords("URLDriver.db","P=$(PREFIX),R=cam1:,PORT=$(PORT),ADDR=0,TIMEOUT=1")

asynSetTraceIOMask($(PORT), 0, 2)
#asynSetTraceMask($(PORT), 0, 0xFF)
#asynSetTraceFile($(PORT), 0, "asynTrace.out")
#asynSetTraceInfoMask($(PORT), 0, 0xf)

### Create a standard arrays plugin
NDStdArraysConfigure("Image1", 5, 0, $(PORT), 0, 0)
### Use this line for 8-bit or 16-bit data
dbLoadRecords("NDStdArrays.template", "P=$(PREFIX),R=image1:,PORT=Image1,ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT),TYPE=Int16,FTVL=SHORT,NELEMENTS=$(NELEMENTS)")

iocInit()
