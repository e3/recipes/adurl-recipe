# ADURL conda recipe

Home: "https://github.com/areaDetector/ADURL"

Package license: EPICS Open License

Recipe license: BSD 3-Clause

Summary: EPICS areaDetector driver for reading images from a URL
